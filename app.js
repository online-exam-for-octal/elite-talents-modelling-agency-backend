const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require("cors");
require("dotenv").config();

const users = require("./routes/users");
const talents = require("./routes/talents");

const app = express();
const port = process.env.PORT || 5000;

mongoose
  .connect(process.env.MONGO_ATLAS, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  })
  .catch((err) => {
    console.log(err);
  });

app.use(cors());
app.use(bodyParser.json());

app.get("/home", (req, res) => {
  res.send({
    message: "Home page",
  });
});

app.use("/users", users);
app.use("/talents", talents);
app.use("/public", express.static("assets/images"));

app.use((err, req, res, next) => {
  res.status(400).send({
    error: err.message,
  });
});

app.listen(port, () => {
  console.log(`App is running on port ${port}`);
});
