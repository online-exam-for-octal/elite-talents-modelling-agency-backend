const router = require("express").Router();
const Talent = require("./../models/Talent");

const multer = require("multer");
const passport = require("passport");
const jwt = require("jsonwebtoken");
require("./../passport-setup");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "assets/images");
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + "-" + file.originalname);
  },
});

const upload = multer({ storage });

router.post("/audition", upload.single("imageOne"), (req, res, next) => {
  //   console.log(req.body);
  if (req.file) {
    req.body.imageOne = req.file.filename;
    // req.body.imageTwo = req.images[1].filename;
  }

  //   req.body.imageTwo = req.imageTwo.filename;

  if (!req.body.firstname) {
    return res.status(400).send({ error: "Firstname is required." });
  }

  if (!req.body.lastname) {
    return res.status(400).send({ error: "Lastname is required." });
  }

  if (!req.body.email) {
    return res.status(400).send({ error: "Email is required." });
  }

  //   if (!req.body.imageOne || !req.body.imageTwo) {
  //     return res.status(400).send({
  //       error: "You need to upload at least two pictures to audition.",
  //     });
  //   }

  if (
    !req.body.isChecked ||
    req.body.isChecked == false ||
    req.body.isChecked == "false"
  ) {
    return res
      .status(400)
      .send({ error: "You must agree to the site's terms and conditions." });
  }

  Talent.findOne({ email: req.body.email }).then((user) => {
    if (user) {
      return res.status(400).send({
        error: "Someone already auditioned with this email. Please try again.",
      });
    } else {
      //   Talent.create({
      //     firstname: req.body.firstname,
      //     lastname: req.body.lastname,
      //     email: req.body.email,
      //     image: req.body.image,
      //   })
      Talent.create(req.body)
        .then((talent) => res.send(talent))
        .catch(next);
    }
  });
});

router.put("/:id", upload.single("imageTwo"), (req, res, next) => {
  if (req.file) {
    req.body.imageTwo = req.file.filename;
  }

  Talent.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
  })
    .then((user) => res.send(user))
    .catch(next);
});

router.get(
  "/view-talents",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    if (req.user.isAdmin) {
      Talent.find().then((talents) => {
        res.send(talents);
      });
    }
  }
);

module.exports = router;
