const router = require("express").Router();
const User = require("./../models/User");

const multer = require("multer");
const bcrypt = require("bcrypt");
const passport = require("passport");
const jwt = require("jsonwebtoken");
require("./../passport-setup");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "assets/images");
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + "-" + file.originalname);
  },
});

const upload = multer({ storage });

router.post("/register", upload.single("image"), (req, res, next) => {
  if (req.file) {
    req.body.image = req.file.filename;
  }

  if (req.body.password !== req.body.confirmPassword) {
    return res.status(400).send({ error: "Passwords don't match." });
  }

  if (!req.body.firstname) {
    return res.status(400).send({ error: "Firstname is required." });
  }

  if (!req.body.lastname) {
    return res.status(400).send({ error: "Lastname is required." });
  }

  if (!req.body.email) {
    return res.status(400).send({ error: "Email is required." });
  }

  if (!req.body.password) {
    return res.status(400).send({ error: "Password is required." });
  }

  if (!req.body.confirmPassword) {
    return res
      .status(400)
      .send({ error: "You need to confirm your password." });
  }

  if (
    !req.body.isChecked ||
    req.body.isChecked == false ||
    req.body.isChecked == "false"
  ) {
    return res
      .status(400)
      .send({ error: "You must agree to the site's terms and conditions." });
  }

  User.findOne({ email: req.body.email }).then((user) => {
    if (user) {
      return res.status(400).send({
        error: "Email already in use.",
      });
    } else {
      console.log(req.body);
      const saltRounds = 10;

      bcrypt.genSalt(saltRounds, function (err, salt) {
        bcrypt.hash(req.body.password, salt, function (err, hash) {
          req.body.password = hash;

          User.create(req.body)
            .then((user) => res.send(user))
            .catch(next);
        });
      });
    }
  });
});

router.put(
  "/:id",
  upload.single("image"),
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    if (req.file) {
      req.body.image = req.file.filename;
    }

    User.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true,
    })
      .then((user) => res.send(user))
      .catch(next);
  }
);

router.post("/login", (req, res, next) => {
  let { email, password } = req.body;

  if (!req.body.email || !req.body.password) {
    return res.status(400).send({
      error: "Email and password are required to login.",
    });
  }

  User.findOne({ email }).then((user) => {
    if (!user) {
      return res.status(400).send({
        error: "Check credentials.",
      });
    } else {
      bcrypt.compare(password, user.password, function (err, result) {
        if (result) {
          const token = jwt.sign({ _id: user._id }, "secret");
          return res.send({
            message: "Login successful!",
            token,
            user: {
              _id: user._id,
              firstname: user.firstname,
              lastname: user.lastname,
              email: user.email,
              image: user.image,
              isAdmin: user.isAdmin,
            },
          });
        } else {
          return res.status(400).send({
            error: "Check Credentials",
          });
        }
      });
    }
  });
});

router.get(
  "/profile",
  passport.authenticate("jwt", { session: false }),
  (req, res, next) => {
    res.send({
      _id: req.user._id,
      firstname: req.user.firstname,
      lastname: req.user.lastname,
      email: req.user.email,
      image: req.user.image,
      isAdmin: req.user.isAdmin,
    });
  }
);

module.exports = router;
