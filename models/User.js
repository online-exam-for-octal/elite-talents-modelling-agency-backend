const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  firstname: {
    type: String,
    trim: true,
    required: [true, "First name is required."],
  },
  lastname: {
    type: String,
    trim: true,
    required: [true, "Last name is required."],
  },
  email: {
    type: String,
    required: [true, "Email address is required."],
  },
  password: {
    type: String,
    minlength: [8, "Password should be at least 8 characters in length."],
    required: [true, "Password is required."],
  },
  confirmPassword: {
    type: String,
    minlength: [8, "Password should be at least 8 characters in length"],
    required: [true, "Password verification is required."],
  },
  image: {
    type: String,
  },
  isAdmin: {
    type: Boolean,
    default: false,
    required: [true, "Status required."],
  },
});

const User = mongoose.model("user", UserSchema);

module.exports = User;
