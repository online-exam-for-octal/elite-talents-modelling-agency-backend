const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const TalentSchema = new Schema({
  firstname: {
    type: String,
    trim: true,
    required: [true, "First name is required."],
  },
  lastname: {
    type: String,
    trim: true,
    required: [true, "Last name is required."],
  },
  email: {
    type: String,
    required: [true, "Email address is required."],
  },
  imageOne: {
    type: String,
    required: [true, "Model photos are required."],
  },
  imageTwo: {
    type: String,
  },
  // images: {
  //   type: [String],
  // },
});

const Talent = mongoose.model("talent", TalentSchema);

module.exports = Talent;
